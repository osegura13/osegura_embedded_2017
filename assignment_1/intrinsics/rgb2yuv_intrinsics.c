/**
 * @file rgb2yuv_c.c
 * @author Oscar Segura
 * @date Feb 24th 2017
 * @brief File containing Assignment 1.
 *
 * In this code we are converting a rgb raw image to a yuv image.
 * results can be confirmed in this link: 
 * @see http://rawpixels.net/
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/time.h>
#include <arm_neon.h>

int c;
char *ivalue = NULL;
char *ovalue = NULL;

//!  A test class. 
/*!
  A more elaborate class description.
*/
int rgb2yuv_conversion(char *input,char *output);

void rgb2yuv (char *input_image, char *output_image){


  printf ("Estoy en rgb2yuv y mis argumentos son %s y %s\n",input_image, output_image);	
    FILE *file;
    if (file = fopen(input_image, "r")){
        fclose(file);
        printf("El archivo %s si existe.\n", input_image);

		printf("El archivo de salida sera %s \n", output_image);

		rgb2yuv_conversion(input_image, output_image);
/*! \brief Brief description.
 *         Brief description continued.
 *
 *  Detailed description starts here.
 */

////////////////////////////////////////////////////////////////////////////////////////

/** this is just a test **/







////////////////////////////////////////////////////////////////////////////////////////
    }
    else{
        printf("ERROR: el archivo %s no existe.\n", input_image);
    }
}





int main( int argc, char **argv){

struct timeval begin, end;
gettimeofday(&begin, NULL);

  while((c=getopt(argc, argv, "ahi:o:"))!=-1)
    switch(c)
	{
  	  case 'i':
		ivalue = optarg;
	  	break;	// End of case s

	  case 'o':
		ovalue = optarg;
		break;	// End of Case b

	  case 'a':
  	    printf("\n************************* dbugger tool *************************\n\n");
	    printf("	       Instituto Tecnológico de Costa Rica\n");
	    printf("                     Maestria en electrónica\n");
	    printf("   		 Enfasis en sistemas empotrados\n\n");
	    printf("  Authors: Oscar Segura            Mario Vargas\n");
	    printf("  email:   osegura13@gmail.com	   m.vargas.cr@ieee.com\n");
	    printf("  Phone:   (506) 8837 2476	   (506) 8817 8574\n\n");
	  break;  

	  case 'h':
	    printf("Tool usage: \n\n ./rgb2yuv [ -i RGBfile ] [ -o YUVfile ] [ -h ] [ -a ]\n\n");
	    printf("  -i RGBfile specifies the RGB file to be converted. \n");
	    printf("  -o YUVfile specifies the output file name.\n");
	    printf("  -a ​Displays the information of the author of the program.\n");
	    printf("  -h​ Displays the usage message to let the user know how to execute the application.\n");
  	  break;  

	  case '?':	
	    if (optopt == 'i'||optopt == 'o')
              fprintf (stderr, "\n");
            else if (isprint (optopt))
              fprintf (stderr, "Opcion desconocida `-%c'", optopt);
            else
              fprintf (stderr, "Unknown option character`\\x%x'.\n", optopt);
          return 1;

	  default:
	    fprintf (stderr, "Unknown option `\\x%x'.\n", c);
	    abort ();
	}
  if(ivalue == NULL)
      printf("Debe especificar un archivo de entrada para convertir con la opcion -i. \n");
  else if(ovalue == NULL)
      printf("Debe especificar un archivo de salida con la opcion -o. \n");
  else 
      rgb2yuv(ivalue, ovalue);    //!< a member function.

//getchar();   //<-- para comprobar funcionamiento del tiempo
gettimeofday(&end, NULL);
int x = ((end.tv_sec * 1000000 + end.tv_usec) - (begin.tv_sec * 1000000 + begin.tv_usec));
printf("Tiempo transcurrido en us: %d \n", x);

  getchar();
  return 0;
}



//extra line


/****************************************************************
 * @file rgb2yuv_conversion(char *input, char *output)
 * @author Mario Vargas
 * @date Mar 3rd 2017
 * @brief function to convert from rgb to yuv and write to a file.
 *
 * In this code we are converting a rgb raw image to a yuv image.
 * results can be confirmed in this link:
 * @see http://rawpixels.net/
 ****************************************************************/


int rgb2yuv_conversion(char *input,char *output) {

	double y_vector[3] = {
		0.299, 0.587, 0.114,
		};
	double u_vector[3] = {
		-0.147, -0.289, 0.436,
		};
	double v_vector[3] = {
		0.615, -0.515, -0.1,
		};

	unsigned char *temporal;

/**/
	FILE *f = fopen(input, "rb");	// read the image raw file
	fseek(f, 0, SEEK_END);			// looks for the amount of memory to allocate
	long fsize = ftell(f);			// initializes the buffer
	fseek(f, 0, SEEK_SET);			// same as rewind(f);

	uint8_t* raw_data = malloc(fsize + 1);	// allocates the required amount of memory
uint8_t* out_data = malloc(fsize+1);
	fread(raw_data, fsize, 1, f);		// stores the data read from the file into the buffer
	fclose(f);				// close file

	int i=0, j=0;	// counter for the for cycle

	/*
		for(i=0; i<fsize; i++)
		{
	//		printf("%d, ", raw_data[i]);	// prints the contents of the file loaded into the "raw_data" buffer
		} //for
	*/

	//saveToFileBMP( *string, "RGB24.png" );
	FILE *outp = fopen(output, "w");
	fseek(outp, 0, SEEK_SET);			// same as rewind(f);

	char y =0, u=0, v=0, y_nxt=0, y_nxt1=0;
	//double r=0.299, g=0.587, b=0.114, u1=0.492, v1=0.877;
	int row =0;

uint8x8_t temporal1;
unsigned char A[fsize];
unsigned char S[24];
uint8x8x3_t v1;

/**/
//yuv values
uint8x8_t y_1;
uint8x8_t u_1;
uint8x8_t v_1;
//uint8x8_t y_nxt;

//y constants
  uint8x8_t yfac1 = vdup_n_u8 (26);
  uint8x8_t yfac2 = vdup_n_u8 (50);
  uint8x8_t yfac3 = vdup_n_u8 (100);
//u constants
  uint8x8_t ufac1 = vdup_n_u8 (15);//neg
  uint8x8_t ufac2 = vdup_n_u8 (29);//neg
  uint8x8_t ufac3 = vdup_n_u8 (44);
//v constants
  uint8x8_t vfac1 = vdup_n_u8 (44);
  uint8x8_t vfac2 = vdup_n_u8 (37);//neg
  uint8x8_t vfac3 = vdup_n_u8 (7);

  uint8x8_t yy = vdup_n_u8 (16);
  uint8x8_t uu = vdup_n_u8 (128);//neg
  uint8x8_t vv = vdup_n_u8 (128);

/**/
//temporal variables

  uint8x8_t tempy1;
  uint8x8_t tempy2;
  uint8x8_t tempy3;

  uint8x8_t tempu1;
  uint8x8_t tempu2;
  uint8x8_t tempu3;

  uint8x8_t tempv1;
  uint8x8_t tempv2;
  uint8x8_t tempv3;

  uint8x8_t tempy_1;
  uint8x8_t tempy_2;
  uint8x8_t tempy_3;

	for(i=0; i<fsize; i+=24) //, raw_data+=24)	//, raw_data+=8*3)
	{
		for(j=0;j<24;j++)		// writes a 24 bytes chunk for processing.
		    S[j]=raw_data[i+j];

		v1 = vld3_u8(S);		// split RGB into R G B

tempy1 = vmul_u8(v1.val[0], yfac1);	// multiply R component by a constant
tempy2 = vmul_u8(v1.val[1], yfac2);	// multiply G component by a constant
tempy3 = vmul_u8(v1.val[2], yfac3);	// multiply B component by a constant

tempy2 = vadd_u8(tempy1, tempy2);
tempy3 = vadd_u8(tempy2, tempy3);
y_1 = vadd_u8(tempy3, yy);		// y component generation


tempu1 = vmul_u8(v1.val[0], ufac1);
tempu1 = vmul_u8(v1.val[1], ufac2);
tempu1 = vmul_u8(v1.val[2], ufac3);

tempy2 = vadd_u8(tempu1, tempu2);
tempy3 = vadd_u8(tempu2, tempu3);
u_1 = vadd_u8(tempu3, uu);		// u component generation
	

tempv1 = vmul_u8(v1.val[0], vfac1);
tempv1 = vmul_u8(v1.val[1], vfac2);
tempv1 = vmul_u8(v1.val[2], vfac3);

tempv2 = vadd_u8(tempv1, tempv2);
tempv3 = vadd_u8(tempv2, tempv3);
v_1 = vadd_u8(tempv3, vv);		// v component generation


v1.val[0]=u_1;				
v1.val[1]=y_1;
v1.val[2]=v_1;
		vst3_u8(S, v1);


for(j=0;j<24;j++)			// writes converted stream
	fputc(S[j],outp);




		}	// for
//printf("i=%d,",i+j);
//for(i=0;i<fsize;i++)
//	fprintf(outp, "%u", A[i]);

	fclose(outp);
	raw_data[fsize] = 0;
	free(raw_data);

}



