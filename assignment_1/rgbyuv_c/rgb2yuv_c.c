/****************************************************************
 * @file rgb2yuv_conversion(char *input, char *output)
 * @authors Mario Vargas, Oscar Segura
 * @date Mar 10th 2017
 * @brief function to convert from rgb to yuv and write to a file.
 *
 * In this code we are converting a rgb raw image to a yuv image.
 * results can be confirmed in this link:
 * @see http://rawpixels.net/
 ****************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/time.h>

int c;
char *ivalue = NULL;
char *ovalue = NULL;

int rgb2yuv_conversion(char *input,char *output) {

	double y_vector[3] = {
		//0.299, 0.587, 0.114,
		0.257, 0.504, 0.098,
		};
	double u_vector[3] = {
		//-0.147, -0.289, 0.436,
		-0.148, -0.291, 0.439,
		};
	double v_vector[3] = {
		//0.615, -0.515, -0.1,
		0.439, -0.368, -0.071,
		};

	unsigned char *temporal;

	FILE *f ; 					// read the image raw file
	if (f = fopen(input, "rb")){   			// check if input file exists
		fseek(f, 0, SEEK_END);			// looks for the amount of memory to allocate
		long fsize = ftell(f);			// initializes the buffer
		fseek(f, 0, SEEK_SET);			// same as rewind(f);

		unsigned char *raw_data = malloc(fsize + 1);	// allocates the required amount of memory
		fread(raw_data, fsize, 1, f);		// stores the data read from the file into the buffer
		fclose(f);				// close file

		int i=0;				// counter for the for cycle

		FILE *outp = fopen(output, "w");
		fseek(outp, 0, SEEK_SET);		// same as rewind(f);

		char y =0, u=0, v=0, y_nxt=0, y_nxt1=0;
		int row =0;
		for(i=0; i<fsize; i+=3)
			{
			y=y_vector[0]*raw_data[i]+y_vector[1]*raw_data[i+1]+y_vector[2]*raw_data[i+2]+16;	// Y component
			u=u_vector[0]*raw_data[i]+u_vector[1]*raw_data[i+1]+u_vector[2]*raw_data[i+2]+128;	// U component
			v=v_vector[0]*raw_data[i]+v_vector[1]*raw_data[i+1]+v_vector[2]*raw_data[i+2]+128;	// V component
			y_nxt=y_vector[0]*raw_data[i+3]+y_vector[1]*raw_data[i+4]+y_vector[2]*raw_data[i+5];

			fprintf(outp,"%c",y);
			fprintf(outp,"%c",u);
			fprintf(outp,"%c",v);
			fprintf(outp,"%c",y_nxt);				//generates the output file
			}	

		fclose(outp);							// close the generated file
		raw_data[fsize] = 0;
		free(raw_data);							// releases memory
		printf("Se genero el archivo %s\n", output);							
	}
  	else{
		printf("ERROR: el archivo %s no existe.\n", input);		// If input file does not exists 
	}									// generates an error
}

int main( int argc, char **argv){

struct timeval begin, end;
gettimeofday(&begin, NULL);

  while((c=getopt(argc, argv, "ahi:o:"))!=-1)
    switch(c)
	{
  	  case 'i':
		ivalue = optarg;
	  	break;	// End of case i

	  case 'o':
		ovalue = optarg;
		break;	// End of Case o

	  case 'a':
  	    printf("\n************************* dbugger tool *************************\n\n");
	    printf("	       Instituto Tecnológico de Costa Rica\n");
	    printf("                     Maestria en electrónica\n");
	    printf("   		 Enfasis en sistemas empotrados\n\n");
	    printf("  Authors: Oscar Segura            Mario Vargas\n");
	    printf("  email:   osegura13@gmail.com	   m.vargas.cr@ieee.com\n");
	    printf("  Phone:   (506) 8837 2476	   (506) 8817 8574\n\n");
	  break;  

	  case 'h':
	    printf("Tool usage: \n\n ./rgb2yuv [ -i RGBfile ] [ -o YUVfile ] [ -h ] [ -a ]\n\n");
	    printf("  -i RGBfile specifies the RGB file to be converted. \n");
	    printf("  -o YUVfile specifies the output file name.\n");
	    printf("  -a ​Displays the information of the author of the program.\n");
	    printf("  -h​ Displays the usage message to let the user know how to execute the application.\n");
  	  break;  

	  case '?':	
	    if (optopt == 'i'||optopt == 'o')
              fprintf (stderr, "\n");
            else if (isprint (optopt))
              fprintf (stderr, "Opcion desconocida `-%c'", optopt);
            else
              fprintf (stderr, "Unknown option character`\\x%x'.\n", optopt);
          return 1;

	  default:
	    fprintf (stderr, "Unknown option `\\x%x'.\n", c);
	    abort ();
	}
  if(ivalue == NULL)
      printf("Debe especificar un archivo de entrada para convertir con la opcion -i. \n");
  else if(ovalue == NULL)
      printf("Debe especificar un archivo de salida con la opcion -o. \n");
  else 
      rgb2yuv_conversion(ivalue, ovalue); 

  gettimeofday(&end, NULL);
  int x = ((end.tv_sec * 1000000 + end.tv_usec) - (begin.tv_sec * 1000000 + begin.tv_usec));
  printf("Tiempo transcurrido en us: %d \n", x);
  return 0;
}
